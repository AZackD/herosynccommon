﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon
{
    public class PlayerData : IDarkRiftSerializable
    {
        public enum PlayerColor
        {
            ORANGE = 0,
            BLUE = 1
        }

        public short influence;
        public PlayerColor playerColor;
        public Deck playerDeck;
        public List<CardData> cardsInHand;
        public CharacterData playerHero;
        public int playerId;
        public string username;

        public void Deserialize(DeserializeEvent e)
        {
            influence = e.Reader.ReadInt16();
            playerColor = (PlayerColor) e.Reader.ReadInt32();
            bool hasDeck = e.Reader.ReadBoolean();
            if (hasDeck)
            {
                playerDeck = e.Reader.ReadSerializable<Deck>();
            }

            int handCount = e.Reader.ReadInt32();
            cardsInHand = new List<CardData>();
            for (int i = 0; i < handCount; i++)
            {
                cardsInHand.Add(e.Reader.ReadSerializable<CardData>());
            }

            bool hasHero = e.Reader.ReadBoolean();
            if (hasHero)
            {
                playerHero = e.Reader.ReadSerializable<CharacterData>();
            }

            playerId = e.Reader.ReadInt32();
            username = e.Reader.ReadString();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(influence);
            e.Writer.Write((int)playerColor);
            bool hasDeck = playerDeck != null;
            e.Writer.Write(hasDeck);
            if (hasDeck)
            {
                e.Writer.Write(playerDeck);
            }

            e.Writer.Write(cardsInHand.Count);
            foreach (CardData data in cardsInHand)
            {
                e.Writer.Write(data);
            }

            bool hasHero = playerHero != null;
            e.Writer.Write(hasHero);
            if (hasHero)
            {
                e.Writer.Write(playerHero);
            }

            e.Writer.Write(playerId);
            e.Writer.Write(username);
        }
    }
}
