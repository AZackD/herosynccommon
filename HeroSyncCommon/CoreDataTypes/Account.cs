﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon
{
    public class Account : IDarkRiftSerializable
    {
        public short accountID;
        
        public string accountName;
        public PlayerData playerData;

        public void Deserialize(DeserializeEvent e)
        {
            accountID = e.Reader.ReadInt16();
            accountName = e.Reader.ReadString();
            bool hasPlayerData = e.Reader.ReadBoolean();
            if (hasPlayerData)
            {
                playerData = e.Reader.ReadSerializable<PlayerData>();
            }
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(accountID);
            e.Writer.Write(accountName);
            bool hasPlayerData = playerData != null;
            e.Writer.Write(hasPlayerData);
            if (hasPlayerData)
            {
                e.Writer.Write(playerData);
            }
        }

        public override bool Equals(object obj)
        {
            if (obj != null && this.GetType() == obj.GetType())
            {
                Account otherAccount = (Account)obj;
                return this.accountID == otherAccount.accountID;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return accountID;
        }
    }
}
