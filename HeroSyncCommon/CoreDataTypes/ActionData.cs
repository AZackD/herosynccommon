﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HeroSyncCommon.CoreDataTypes
{
    /**
     * This class represents one atomic change to the game's state taken by a player. 
     */
    public class ActionData
    {
        public string desc;
        public Guid id;
        public ActionTypeEnum type;
        public Object[] data;

        /**
         * These are referenced by the database and event trigger factories. Do not change their numerical values. Note that No Trigger is used to set up
         * event triggers that do not activate off a specific type of action trigger.
         */
        public enum CoreActionTriggerTypeEnum
        {
            CardMoveIntent = 0,
            CardMoveActivation = 1,
            CardStatChangeIntent = 2,
            CardStatChangeActivation = 3,
            CardEffectIntent = 4,
            CardEffectActivation = 5,
            HeroEffectIntent = 6,
            HeroEffectUsed = 7,
            HeroLevelUpIntent = 8,
            HeroLevelUpFinish = 9,
            EndTurnIntent = 10,
            EndTurnFinish = 11,
            StartTurnIntent = 12,
            StartTurnFinish = 13,
            InfChangeIntent = 14,
            InfChangeFinish = 15,
            WhenSummonIntent = 16,
            AfterSummonIntent = 17,
            WhenSummonSuccessful = 18,
            AfterSummonSuccessful = 19,
            WhenAttackTarget = 20,
            AfterAttackTarget = 21,
            WhenAttack = 22,
            AfterAttack = 23,
            WhenKilled = 24,
            AfterKilled = 25,
            WhenSacrificed = 26,
            AfterSacrificed = 27,
            WhenDrawCard = 28,
            AfterDrawCard = 29,
            WhenCardMovedToLocation = 34,
            WhenCardMovedToZone = 35,
            NoTrigger = 999
        }

        public enum ActionTypeEnum
        {
            MoveCard = 0,
            SummonFollower = 1,
            KillFollower = 2,
            SacrificeFollower = 3,
            InfChange = 4,
            EffectActivate = 5,
            HeroTalentActivate = 6,
            HeroLevelUp = 7, 
            EndTurn = 8,
            Attack = 9,
            ModifyCard = 10,
            UpdateFrontEndSummon = 11,
            WeakenFollower = 12,
            PlayDrive = 13,
            TargetFollower = 14,
            TargetHero = 15,
            CompositeAction = 16,
            DrawCard = 17,
            NoAction = 999
        }
    }
}
