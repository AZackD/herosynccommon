﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon
{
    public class CharacterData : IDarkRiftSerializable
    {
        public string characterName;
        public short characterId;
        public short gameId;
        public short firstLevelOption;
        public short secondLevelOption;
        public short thirdLevelOption;
        public short level;

        public void Deserialize(DeserializeEvent e)
        {
            characterName = e.Reader.ReadString();
            characterId = e.Reader.ReadInt16();
            gameId = e.Reader.ReadInt16();
            firstLevelOption = e.Reader.ReadInt16();
            secondLevelOption = e.Reader.ReadInt16();
            thirdLevelOption = e.Reader.ReadInt16();
            level = e.Reader.ReadInt16();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(characterName);
            e.Writer.Write(characterId);
            e.Writer.Write(gameId);
            e.Writer.Write(firstLevelOption);
            e.Writer.Write(secondLevelOption);
            e.Writer.Write(thirdLevelOption);
            e.Writer.Write(level);
        }

        public enum CharacterAction
        {
            selectBaseOption,
            selectLevel1Option,
            selectLevel2Option,
            selectLevel3Option,
            useBaseOption,
            useLevel1Option,
            useLevel2Option,
            useLevel3Option
        }
    }
}
