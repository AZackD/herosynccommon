﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon.GameManagement
{
    public class JoinRoomRequest : IDarkRiftSerializable
    {
        public int roomID;
        public short accountID;

        public void Deserialize(DeserializeEvent e)
        {
            DarkRiftReader reader = e.Reader;
            roomID = reader.ReadInt32();
            accountID = reader.ReadInt16();
        }

        public void Serialize(SerializeEvent e)
        {
            DarkRiftWriter writer = e.Writer;
            writer.Write(roomID);
            writer.Write(accountID);
        }
    }
}
