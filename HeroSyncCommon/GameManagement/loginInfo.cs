﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon.GameManagement
{
    public class LoginInfo : IDarkRiftSerializable
    {
        public string username;
        public short id;

        public void Deserialize(DeserializeEvent e)
        {
            DarkRiftReader r = e.Reader;
            username = r.ReadString();
            id = r.ReadInt16();
        }

        public void Serialize(SerializeEvent e)
        {
            DarkRiftWriter w = e.Writer;
            w.Write(username);
            w.Write(id);
        }
    }
}
