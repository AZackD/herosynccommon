﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon.NetworkActions
{
    public class SetCounter : IDarkRiftSerializable
    {
        public CardData cardToSet;
        public PlayerField.Zone targetedZone;
        public PlayerData.PlayerColor player;

        public void Deserialize(DeserializeEvent e)
        {
            cardToSet = e.Reader.ReadSerializable<CardData>();
            targetedZone = (PlayerField.Zone)e.Reader.ReadInt32();
            player = (PlayerData.PlayerColor) e.Reader.ReadInt32();
        }

        public void Serialize(SerializeEvent e)
        {
            DarkRiftWriter w = e.Writer;
            w.Write(cardToSet);
            w.Write((int)targetedZone);
            w.Write((int)player);
        }
    }
}
