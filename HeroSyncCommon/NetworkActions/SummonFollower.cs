﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon.NetworkActions
{
    public class SummonFollower : IDarkRiftSerializable
    {
        public CardData cardToSummon;
        public PlayerField.Zone targetedZone;
        public PlayerData.PlayerColor player;

        public void Deserialize(DeserializeEvent e)
        {
            DarkRiftReader r = e.Reader;
            cardToSummon = r.ReadSerializable<CardData>();
            targetedZone = (PlayerField.Zone) r.ReadInt32();
            player = (PlayerData.PlayerColor) r.ReadInt32();
        }

        public void Serialize(SerializeEvent e)
        {
            DarkRiftWriter w = e.Writer;
            w.Write(cardToSummon);
            w.Write((int)targetedZone);
            w.Write((int)player);
        }
    }
}
