﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon.NetworkActions
{
    public class GameOver : IDarkRiftSerializable
    {

        public string winnerName;
        public PlayerData.PlayerColor winningColor;

        public void Deserialize(DeserializeEvent e)
        {
            winnerName = e.Reader.ReadString();
            winningColor = (PlayerData.PlayerColor) e.Reader.ReadInt32();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(winnerName);
            e.Writer.Write((int) winningColor);
        }
    }
}
