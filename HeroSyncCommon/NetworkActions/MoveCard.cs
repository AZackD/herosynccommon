﻿using DarkRift;

namespace HeroSyncCommon.NetworkActions
{
    public class MoveCard : IDarkRiftSerializable
    {
        public CardData cardToMove;
        public PlayerField.Zone location;
        public bool showCardBeforeMoving;
        public bool cardHidden;

        public void Serialize(SerializeEvent e)
        {
            DarkRiftWriter w = e.Writer;
            w.Write(cardToMove);
            w.Write((int) location);
            w.Write(showCardBeforeMoving);
            w.Write(cardHidden);
        }

        public void Deserialize(DeserializeEvent e)
        {
            DarkRiftReader r = e.Reader;
            cardToMove = r.ReadSerializable<CardData>();
            location = (PlayerField.Zone) r.ReadInt32();
            showCardBeforeMoving = r.ReadBoolean();
            cardHidden = r.ReadBoolean();
        }
    }
}