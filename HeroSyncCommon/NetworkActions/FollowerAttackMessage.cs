﻿using DarkRift;

namespace HeroSyncCommon.NetworkActions
{
    public class FollowerAttackMessage : IDarkRiftSerializable
    {
        public CardData attackingCard;
        public CardData targetedCard;
        public CharacterData targetedHero;
        public TargetType targetType;
        public PlayerData attackingPlayer;

        public void Deserialize(DeserializeEvent e)
        {
            DarkRiftReader r = e.Reader;
            attackingCard = r.ReadSerializable<CardData>();
            TargetType targetType = (TargetType) r.ReadInt32();
            if (targetType == TargetType.CARD)
            {
                targetedCard = r.ReadSerializable<CardData>();
            }
            else
            {
                targetedHero = r.ReadSerializable<CharacterData>();
            }

            attackingPlayer = r.ReadSerializable<PlayerData>();
        }

        public void Serialize(SerializeEvent e)
        {
            DarkRiftWriter w = e.Writer;
            w.Write(attackingCard);
            w.Write((int) targetType);
            if (targetType == TargetType.CARD)
            {
                w.Write(targetedCard);
            }
            else
            {
                w.Write(targetedHero);
            }
            w.Write(attackingPlayer);
        }

        public enum TargetType
        {
            CARD,
            HERO
        }
    }
}