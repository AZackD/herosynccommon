﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon.NetworkActions
{ public class CardSet : IDarkRiftSerializable
    {
        public int cardCount;
        public List<CardData> cardSet;
        public void Deserialize(DeserializeEvent e)
        {
            DarkRiftReader r = e.Reader;
            cardCount = r.ReadInt32();
            CardData[] arrayData = new CardData[cardCount];

            for (int i = 0; i < cardCount; i++)
            {
                arrayData[i] = r.ReadSerializable<CardData>();
            }

            cardSet = arrayData.ToList();
        }

        public void Serialize(SerializeEvent e)
        {
            DarkRiftWriter w = e.Writer;
            w.Write(cardCount);
            foreach (CardData c in cardSet)
            {
                w.Write(c);
            }
        }
    }
}
