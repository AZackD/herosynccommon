﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroSyncCommon.GameLogic
{
    public static class ZoneConverter
    {
        public static CardLocation ToLocation(PlayerField.Zone zone)
        {
            switch (zone)
            {
                case PlayerField.Zone.OrangeFallenArea:
                case PlayerField.Zone.BlueFallenArea:
                    return CardLocation.FallenArea;

                case PlayerField.Zone.OrangeDeckArea:
                case PlayerField.Zone.BlueDeckArea:
                    return CardLocation.Deck;
                case PlayerField.Zone.BlueCounterSlot1:
                case PlayerField.Zone.BlueCounterSlot2:
                case PlayerField.Zone.OrangeCounterSlot1:
                case PlayerField.Zone.OrangeCounterSlot2:
                case PlayerField.Zone.BlueFollowerSlot1:
                case PlayerField.Zone.BlueFollowerSlot2:
                case PlayerField.Zone.BlueFollowerSlot3:
                case PlayerField.Zone.OrangeFollowerSlot1:
                case PlayerField.Zone.OrangeFollowerSlot2:
                case PlayerField.Zone.OrangeFollowerSlot3:
                    return CardLocation.Field;
                case PlayerField.Zone.BlueHand:
                case PlayerField.Zone.OrangeHand:
                    return CardLocation.Hand;
                default:
                    return CardLocation.Erased;
            }
        }

        public static PlayerData.PlayerColor ToPlayerColor(PlayerField.Zone zone)
        {
            switch (zone)
            {
                case PlayerField.Zone.BlueDeckArea:
                case PlayerField.Zone.BlueFallenArea:
                case PlayerField.Zone.BlueCounterSlot1:
                case PlayerField.Zone.BlueCounterSlot2:
                case PlayerField.Zone.BlueFollowerSlot1:
                case PlayerField.Zone.BlueFollowerSlot2:
                case PlayerField.Zone.BlueFollowerSlot3:
                case PlayerField.Zone.BlueErasedArea:
                case PlayerField.Zone.BlueHand:
                    return PlayerData.PlayerColor.BLUE;
                case PlayerField.Zone.OrangeDeckArea:
                case PlayerField.Zone.OrangeFallenArea:
                case PlayerField.Zone.OrangeCounterSlot1:
                case PlayerField.Zone.OrangeCounterSlot2:
                case PlayerField.Zone.OrangeFollowerSlot1:
                case PlayerField.Zone.OrangeFollowerSlot2:
                case PlayerField.Zone.OrangeFollowerSlot3:
                case PlayerField.Zone.OrangeErasedArea:
                case PlayerField.Zone.OrangeHand:    
                    return PlayerData.PlayerColor.ORANGE;
                default:
                    Console.Error.Write("Tried to convert zone to color, but zone was missing from table.");
                    return PlayerData.PlayerColor.BLUE;
            }
        }

        public static bool IsFollowerZone(PlayerField.Zone zone, PlayerData.PlayerColor color)
        {
            if (color == PlayerData.PlayerColor.BLUE)
            {
                return zone == PlayerField.Zone.BlueFollowerSlot1 || zone == PlayerField.Zone.BlueFollowerSlot2 ||
                       zone == PlayerField.Zone.BlueFollowerSlot3;
            } else if (color == PlayerData.PlayerColor.ORANGE)
            {
                return zone == PlayerField.Zone.OrangeFollowerSlot1 || zone == PlayerField.Zone.OrangeFollowerSlot2 ||
                       zone == PlayerField.Zone.OrangeFollowerSlot3;
            }

            return false;
        }

        public static bool IsCounterZone(PlayerField.Zone zone, PlayerData.PlayerColor color)
        {
            if (color == PlayerData.PlayerColor.BLUE)
            {
                return zone == PlayerField.Zone.BlueCounterSlot1 || zone == PlayerField.Zone.BlueCounterSlot2;
            }
            else if (color == PlayerData.PlayerColor.ORANGE)
            {
                return zone == PlayerField.Zone.OrangeCounterSlot1 || zone == PlayerField.Zone.OrangeCounterSlot2;
            }
            return false;
        }

        public static PlayerField.Zone OpposingZone(PlayerField.Zone zone)
        {
            PlayerData.PlayerColor owningPlayerColor = ToPlayerColor(zone);
            int swapValue = 8;
            if (owningPlayerColor == PlayerData.PlayerColor.ORANGE)
            {
                swapValue = -swapValue;
            }
            PlayerField.Zone opposingZone = (PlayerField.Zone) (zone + swapValue);
            Console.Write("Converted zone {0} to zone {1}", zone.ToString(), opposingZone.ToString());
            return opposingZone;
        }
    }
}
